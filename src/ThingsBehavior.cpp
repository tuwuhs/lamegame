/* ThingsBehavior
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "ThingsBehavior.h"

bool ThingsBehavior::loadImages(std::string path) {
	ofDirectory dir;
	dir.allowExt("png");

	// Load primary textures
	dir.listDir(path);
	if (dir.numFiles() <= 0) {
		return false;
	}
	for (int i=0; i<dir.numFiles(); i++) {
		ofLog() << "Loaded image: " << dir.getPath(i);
		ofImage im;
		im.loadImage(dir.getPath(i));
		textures.push_back(im.getTextureReference());
	}

	// Load secondary textures
	dir.listDir(path + "/hit");
	for (int i=0; i<dir.numFiles(); i++) {
		ofLog() << "Loaded image: " << dir.getPath(i);
		ofImage im;
		im.loadImage(dir.getPath(i));
		texturesHit.push_back(im.getTextureReference());
	}
	
	return true;
}

void ThingsBehavior::hitTestAndUpdate(ofPolyline& line) {
	std::vector<FallingThing>::iterator it;
	for (it = things.begin(); it != things.end(); ++it) {
		it->update();
		if (it->hitTest(line)) {
			hit();
			it->switchTexture(true);
			it->setFalling(false);
			it->hide();
		}
		if (it->isHit()) {
			if (!it->isVisible()) {
				it = things.erase(it) - 1;
			}
		} else if (it->hasReachedGround()) {
			it->setFalling(false);
			it->hide();
			if (!it->isVisible()) {
				it = things.erase(it) - 1;
			}
		}
	}
}

void ThingsBehavior::hitTestAndUpdate(std::vector<ofPolyline>& lines) {
	std::vector<FallingThing>::iterator it;
	for (it = things.begin(); it != things.end(); ++it) {
		it->update();
		if (it->hitTest(lines)) {
			hit();
			it->switchTexture(true);
			it->setFalling(false);
			it->hide();
		}
		if (it->isHit()) {
			if (!it->isVisible()) {
				it = things.erase(it) - 1;
			}
		} else if (it->hasReachedGround()) {
			it->setFalling(false);
			it->hide();
			if (!it->isVisible()) {
				it = things.erase(it) - 1;
			}
		}
	}

	update();
}

void ThingsBehavior::update() {
	if (bAutoSpawn) {
		if (ofGetElapsedTimeMillis() - getLastSpawnTime() > spawnRate) {
			spawn();
		}
	}
}

void ThingsBehavior::setAutoSpawn(bool a) {
	bAutoSpawn = a;
}

void ThingsBehavior::setSpawnRate(int s) {
	spawnRate = s;
}

void ThingsBehavior::draw() {
	std::vector<FallingThing>::reverse_iterator it;
	for (it = things.rbegin(); it != things.rend(); ++it) {
		it->draw();
	}
}

void ThingsBehavior::spawn() {
	FallingThing t;
	lastSpawnTime = ofGetElapsedTimeMillis();
	t.setPosition(ofRandom(0, ofGetWidth()), 0);
	t.loadTexture(textures[ofRandom(0, textures.size())], texturesHit[ofRandom(0, texturesHit.size())]);
	t.setSize(80);
	t.setHitRadius(40);
	t.setGround(groundLevel);
	t.setFallingSpeed(fallingSpeed);
	t.setAnimationTimeMillis(5000);
	things.push_back(t);
}

unsigned long long ThingsBehavior::getLastSpawnTime() {
	return lastSpawnTime;
};

void ThingsBehavior::setGroundlevel(int g) {
	groundLevel = g;
};

void ThingsBehavior::setFallingSpeed(int f) {
	fallingSpeed = f;
};
