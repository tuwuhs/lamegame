/* Score
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "Score.h"

void Score::loadFont(std::string path) {
	font.loadFont(path, size);
}

ofPoint Score::getPosition() {
	return position;
}

void Score::setPosition(ofPoint& p) {
	position = p;
}

void Score::setPosition(int x, int y) {
	position.x = x;
	position.y = y;
}

void Score::setSize(int s) {
	size = s;
}

int Score::getSize() {
	return size;
}

void Score::draw() {
	ofPushStyle();
	ofSetColor(color);
	font.drawString(ofToString(value), position.x, position.y);
	ofPopStyle();
}

void Score::setColor(ofColor c) {
	color = c;
}

ofColor Score::getColor() {
	return color;
}

void Score::add(int v) {
	value += v;
}

void Score::reset() {
	value = 0;
}
