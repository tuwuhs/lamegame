/* FallingThing
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "FallingThing.h"

void FallingThing::update(void) {
	if (bFalling) {
		unsigned long long t = ofGetElapsedTimeMillis();
		position.y += ((t - lastTimeMillis) * fallingSpeed / 1000);
		if (position.y > ground) {
			bReachedGround = true;
			bFalling = false;
			position.y = ground;
		}
		lastTimeMillis = t;
	}

	Thing::update();
}

void FallingThing::setFallingSpeed(int s) {
	fallingSpeed = s;
}

int FallingThing::getFallingSpeed() {
	return fallingSpeed;
}

bool FallingThing::isFalling() {
	return bFalling;
}

void FallingThing::setFalling(bool f) {
	bFalling = f;
}

void FallingThing::setGround(int g) {
	ground = g;
}

int FallingThing::getGround() {
	return ground;
}

bool FallingThing::hasReachedGround() {
	return bReachedGround;
}
