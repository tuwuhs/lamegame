/* Thing
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"

class Thing {
public:
	Thing() {
		transparency = 255;
		bVisible = true;
		bAnimateToHide = false;
		bAnimateToShow = false;
		bUseSecondaryTexture = false;
		bHit = false;
		bLastHit = false;
	}

	void draw();
	void update();

	bool hitTest(ofPolyline);
	bool hitTest(std::vector<ofPolyline>&);
	bool isHit();

	void setPosition(ofPoint);
	void setPosition(int, int);
	ofPoint getPosition();

	void setSize(int);
	int getSize();
	void setHitRadius(int);
	int getHitRadius();

	void setSize(int, int);
	int getWidth();
	int getHeight();

	void show();
	void hide();
	void setVisible(bool);
	bool isVisible();
	bool isAnimating();

	void setAnimationTimeMillis(int);
	int getAnimationTimeMillis();

	void loadTexture(ofTexture);
	void loadTexture(ofTexture, ofTexture);
	void switchTexture(bool);

protected:
	bool bHit;
	bool bLastHit;

	bool bVisible;
	bool bAnimateToHide;
	bool bAnimateToShow;
	unsigned long long animationTimeStart;
	int animationTime;

	int transparency;
	ofPoint position;
	int radius;
	int hitRadius;

	bool bUseSecondaryTexture;
	ofTexture texPrimary;
	ofTexture texSecondary;
};
