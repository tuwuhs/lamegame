/* FinanceGame
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include "Thing.h"
#include "FallingThing.h"
#include "ThingsBehavior.h"
#include "Score.h"

class Uang50: public ThingsBehavior {
public:
	void hit() {
		if (pScore) {
			pScore->add(50000);
		}
	};
	void setScoreObject(Score* s) {
		pScore = s;
	};
protected:
	Score* pScore;
};

class Uang100: public ThingsBehavior {
public:
	void hit() {
		if (pScore) {
			pScore->add(100000);
		}
	};
	void setScoreObject(Score* s) {
		pScore = s;
	};
protected:
	Score* pScore;
};

class Dynamite: public ThingsBehavior {
public:
	void hit() {
		if (pScore) {
			pScore->add(-50000);
		}
	};
	void setScoreObject(Score* s) {
		pScore = s;
	};
protected:
	Score* pScore;
};

class FinanceGame {
public:
	void setup();
	void update();
	void draw();
	void setLines(std::vector<ofPolyline>&);
	void updateLines(std::vector<ofPolyline>&);

protected:
	std::vector<ofPolyline> lines;

	Uang50 uang50;
	Uang100 uang100;
	Dynamite dynamite;

	Score score;
};
