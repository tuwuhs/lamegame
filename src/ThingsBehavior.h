/* ThingsBehavior
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include "FallingThing.h"

class ThingsBehavior {
public:
	ThingsBehavior() {
		groundLevel = 640;
		fallingSpeed = 200;
		bAutoSpawn = true;
		spawnRate = 100;
	};
	bool loadImages(std::string);
	void hitTestAndUpdate(ofPolyline&);
	void hitTestAndUpdate(std::vector<ofPolyline>&);
	void update();
	void spawn();
	void draw();
	virtual void hit() {};
	unsigned long long getLastSpawnTime();
	void setGroundlevel(int);
	void setFallingSpeed(int);
	void setSpawnRate(int);
	void setAutoSpawn(bool);

protected:
	std::vector<FallingThing> things;
	std::vector<ofTexture> textures;
	std::vector<ofTexture> texturesHit;
	unsigned long long lastSpawnTime;
	int groundLevel;
	int fallingSpeed;
	bool bAutoSpawn;
	int spawnRate;
};
