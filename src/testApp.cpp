#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofBackground(ofColor::black);
	
	game.setup();

	// Create lines
	ofPolyline l;
	l.addVertex(100, 200);
	l.addVertex(400, 300);
	lines.push_back(l);
	l.clear();
	l.addVertex(700, 400);
	l.addVertex(500, 600);
	lines.push_back(l);
}

//--------------------------------------------------------------
void testApp::update(){
	game.updateLines(lines);
	game.update();
}

//--------------------------------------------------------------
void testApp::draw(){
	std::vector<ofPolyline>::iterator it;
	for (it = lines.begin(); it != lines.end(); ++it) {
		it->draw();
	}

	game.draw();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}