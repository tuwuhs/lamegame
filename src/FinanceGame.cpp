/* FinanceGame
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "FinanceGame.h"

void FinanceGame::setup() {
	// Set score object
	uang50.setScoreObject(&score);
	uang100.setScoreObject(&score);
	dynamite.setScoreObject(&score);
	
	// Load textures
	uang50.loadImages("things/50rb");
	uang100.loadImages("things/100rb");
	dynamite.loadImages("things/dynamite");

	// Set spawn rates (in milliseconds)
	uang50.setSpawnRate(1000);
	uang100.setSpawnRate(1600);
	dynamite.setSpawnRate(2100);

	// Set speeds
	uang50.setFallingSpeed(200);
	uang100.setFallingSpeed(250);
	dynamite.setFallingSpeed(225);

	score.loadFont("DejaVuSansMono-Bold.ttf");
	score.setColor(ofColor::green);
	score.setPosition(20, 50);
}

void FinanceGame::setLines(std::vector<ofPolyline>& l) {
	lines = l;
}

void FinanceGame::updateLines(std::vector<ofPolyline>& l) {
	if (lines.size() == 0) {
		setLines(l);
	} else if (lines.size() == l.size()) {
		lines.swap(l);
	}
}

void FinanceGame::update() {
	uang50.hitTestAndUpdate(lines);
	uang100.hitTestAndUpdate(lines);
	dynamite.hitTestAndUpdate(lines);
}

void FinanceGame::draw() {
	uang50.draw();
	uang100.draw();
	dynamite.draw();

	score.draw();
}