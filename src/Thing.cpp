/* Thing
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "Thing.h"

void Thing::update() {
	if (bAnimateToHide) {
		unsigned long long t = ofGetElapsedTimeMillis();
	
		if (transparency > 0) {
			transparency -= ((t-animationTimeStart)*256/animationTime);
		} else {
			transparency = 0;
			bAnimateToHide = false;
			bVisible = false;
		}
	}

	if (bAnimateToShow) {
		unsigned long long t = ofGetElapsedTimeMillis();
	
		if (transparency < 255) {
			transparency += ((t-animationTimeStart)*256/animationTime);
		} else {
			transparency = 255;
			bAnimateToShow = false;
		}
	}
}

void Thing::draw() {
	if (!bVisible) return;

	ofPushStyle();
	ofEnableAlphaBlending();
	ofSetColor(ofColor::white, transparency);
	
	/*
	ofNoFill();
	ofRect(position.x - radius, position.y - radius, radius*2, radius*2);
	ofCircle(position, hitRadius);
	*/

	ofTexture tex;
	if (bUseSecondaryTexture) {
		tex = texSecondary;
	} else {
		tex = texPrimary;
	}

	if (tex.isAllocated()) {
		ofRectangle rectTex;
		rectTex.width = tex.getWidth();
		rectTex.height = tex.getHeight();

		ofRectangle rectBox;
		rectBox.width = radius*2;
		rectBox.height = radius*2;

		rectTex.scaleTo(rectBox, OF_ASPECT_RATIO_KEEP);
		rectTex.x = position.x - rectTex.width/2;
		rectTex.y = position.y - rectTex.height/2;

		tex.draw(rectTex);
	}
	
	ofDisableAlphaBlending();
	ofPopStyle();
}

bool Thing::hitTest(std::vector<ofPolyline>& lines) {
	bHit = false;

	std::vector<ofPolyline>::iterator it;
	for (it = lines.begin(); it != lines.end(); ++it) {
		ofPolyline& line = *it;

		// Only test single line
		if (line.getVertices().size() == 2) {
			// ab is the line, c is this object position
			ofPoint& a = line.getVertices()[0];
			ofPoint& b = line.getVertices()[1];
			ofPoint& c = position;

			ofVec2f ab = b - a;
			ofVec2f ac = c - a;
			ofVec2f bc = c - b;

			float acDotAb = ac.dot(ab);
			float bcDotBa = bc.dot(-ab);

			if (acDotAb > 0.0 && bcDotBa > 0.0) {
				float sinSquared = 1.0 - (acDotAb*acDotAb / (ac.lengthSquared() * ab.lengthSquared()));
				float distanceSquared = ac.lengthSquared() * sinSquared;
				if (distanceSquared <= hitRadius*hitRadius) {
					bHit = true;
					break;
				}
			}
		}
	}

	bool ret = false;
	if (bHit && !bLastHit) {
		ret = true;
	}
	bLastHit = bHit;

	return ret;
}

bool Thing::hitTest(ofPolyline line) {
	bHit = false;

	// Only test single line
	if (line.getVertices().size() == 2) {
		// ab is the line, c is this object position
		ofPoint& a = line.getVertices()[0];
		ofPoint& b = line.getVertices()[1];
		ofPoint& c = position;

		ofVec2f ab = b - a;
		ofVec2f ac = c - a;
		ofVec2f bc = c - b;

		float acDotAb = ac.dot(ab);
		float bcDotBa = bc.dot(-ab);

		if (acDotAb > 0.0 && bcDotBa > 0.0) {
			float sinSquared = 1.0 - (acDotAb*acDotAb / (ac.lengthSquared() * ab.lengthSquared()));
			float distanceSquared = ac.lengthSquared() * sinSquared;
			if (distanceSquared <= hitRadius*hitRadius) {
				bHit = true;
			}
		}
	}

	bool ret = false;
	if (bHit && !bLastHit) {
		ret = true;
	}
	bLastHit = bHit;

	return ret;
}

bool Thing::isHit(void) {
	return bHit;
}

void Thing::show() {
	if (bAnimateToShow && !bAnimateToHide) return;
	animationTimeStart = ofGetElapsedTimeMillis();
	bVisible = true;
	bAnimateToShow = true;
	bAnimateToHide = false;
}

void Thing::hide() {
	if (!bAnimateToShow && bAnimateToHide) return;
	animationTimeStart = ofGetElapsedTimeMillis();
	bAnimateToShow = false;
	bAnimateToHide = true;
}

void Thing::setVisible(bool v) {
	bVisible = v;
}

bool Thing::isVisible() {
	return bVisible;
}

bool Thing::isAnimating() {
	return (bAnimateToShow || bAnimateToHide);
}

void Thing::setAnimationTimeMillis(int t) {
	animationTime = t;
}

int Thing::getAnimationTimeMillis() {
	return animationTime;
}

void Thing::setPosition(ofPoint p) {
	position = p;
}

void Thing::setPosition(int x, int y) {
	position.x = x;
	position.y = y;
}

ofPoint Thing::getPosition() {
	return position;
}

void Thing::setSize(int r) {
	radius = r;
}

int Thing::getSize() {
	return radius;
}

void Thing::setHitRadius(int r) {
	hitRadius = r;
}

int Thing::getHitRadius() {
	return hitRadius;
}

void Thing::loadTexture(ofTexture t) {
	texPrimary = t;
}

void Thing::loadTexture(ofTexture tp, ofTexture ts) {
	texPrimary = tp;
	texSecondary = ts;
}

void Thing::switchTexture(bool b) {
	if (b && texSecondary.isAllocated()) {
		bUseSecondaryTexture = true;
	} else {
		bUseSecondaryTexture = false;
	}
}
