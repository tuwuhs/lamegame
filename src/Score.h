/* Score
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"

class Score {
public:
	Score() {
		size = 24;
	};

	void loadFont(std::string);
	
	void setPosition(ofPoint&);
	void setPosition(int, int);
	ofPoint getPosition();

	void setSize(int);
	int getSize();

	void setColor(ofColor);
	ofColor getColor();

	void draw();

	void add(int);
	void reset();

protected:
	int value;
	ofPoint position;
	int size;
	ofTrueTypeFont font;
	ofColor color;
};
