/* Thing
 * openFrameworks 0073
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include "Thing.h"

class FallingThing: public Thing {
public:
	FallingThing() {
		fallingSpeed = 200;
		ground = 400;
		bFalling = true;
		lastTimeMillis = ofGetElapsedTimeMillis();
		bReachedGround = false;
	};

	void update();
	
	void setFallingSpeed(int);
	int getFallingSpeed();

	bool isFalling();
	void setFalling(bool);

	void setGround(int);
	int getGround();
	bool hasReachedGround();

protected:
	unsigned long long lastTimeMillis;
	bool bFalling;
	int fallingSpeed;
	bool bReachedGround;
	int ground;
};
